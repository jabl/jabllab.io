---
title: About me
comments: false
---

Hi, I'm Janne Blomqvist. I work with scientific computing, designing
supercomputers and trying to help scientists use computational tools.  That
being said, this is my personal blog, and everything here is my personal
opinion and does not constitute any statement by my employer.  By training I'm
a physicist, with a Phd in computational materials science, where I studied
things like heterogeneous catalysis and polymers on metal oxide surfaces.

Also interested in various other issues such as data analysis,
computing in general, sustainability, energy, woodworking, hiking,
boats, and whatnot.
