---
title: Hello World
author: Janne
date: 2018-08-09
---

So this is a blog. Being a closet Haskell fan, I'm experimenting with
Hakyll, and Gitlab pages to host it. Lets see how it turns out.
