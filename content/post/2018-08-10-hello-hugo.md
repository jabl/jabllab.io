---
title: Hello Hugo
author: Janne
date: 2018-08-10
tags: ["hugo"]
---

Despite my closet Haskell fandom, the big problem with using Hakyll
was that it took >1h to build all the prerequisites in the CI/CD
pipeline. This is a bit too much waste to run everytime I push an
update. So instead use Hugo, which has a ready-made image for CI/CD.
