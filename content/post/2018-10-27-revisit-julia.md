---
title: Time to revisit Julia
author: Janne
date: 2018-10-27
tags: ["julia", "programming"]
---

Back when there was a bit of buzz around the [Julia programming
language](https://julialang.org) some years ago, I took a look at it,
and I was quite impressed. I even managed to contribute a tiny bit to
the nascent Julia ecosystem back then. However, for various reasons I
found myself using it very little for real-world usage of it so my
Julia usage sort-of fell off. Now, with the recent release of 1.0 I
think it's time to revisit it. Both the language, and in particular
the ecosystem, has come a long way.

Back then I implemented a Julia version of my own little benchmark
program [tb](https://gitlab.com/jabl/tb), and performance back then
was pretty good, within a factor of two of the Fortran and C++
versions. More recently with the release of Julia 1.0 I updated the
program, tuned it a bit, and now it's roughly on par with
Fortran/C++. Very impressive for a high level dynamic language!

I'm a fan of strong static typing (e.g. Haskell, though by I'm far
from an expert in that), so in that sense Julia doesn't tick all of my
checkboxes. However, for a high-level dynamic environment dynamic
typing is probably the best choice, and Julia does dynamic typing
right. Also, the choice of having mutable variables by default might
raise the neck hair of functional programming fans, but in numerical
computing one often works with large dense arrays, and sacrificing
mutability comes at a pretty high cost there.

So what I'm saying is that now is the time to try again to get into
Julia and integrate it into my workflow. Lets see how it goes.
