---
title: Making a bootable FreeDOS USB key from the Linux command line
author: Janne
date: 2021-04-21
draft: false
tags: ["FreeDOS", "Linux", "USB", "Grub 2"]
---

# Introduction

I had to make a bootable USB key in order to do a BIOS update on a machine. The BIOS update was delivered as a MS-DOS executable. I found various posts around the net how do it, some using the old GRUB 1 rather than the current GRUB 2, others leaving some things out etc. Here's what I did to make it work.

# Get a USB key and format it

Figure out which device name the USB key has (e.g. look at `df` or `dmesg` output), and partition the device. I'm going to use `${DISK}` to refer to the device, e.g. `/dev/sdh`.

I'm not sure FreeDOS understands FAT32, so I used FAT16 which is limited to 4G, so I created a single 4G partition and left the rest unused.

    parted ${DISK}

Then create a FAT16 filesystem on the partition.

    mkdosfs -F16 ${DISK}1

(Note the "1" above, which refers to the first partition and not the disk as a whole)

# Install GRUB to the MBR of the USB key

This uses GRUB 2 (on Ubuntu)

    grub-install --target=i386-pc --boot-directory=${MOUNTPOINT}/boot --removable ${DISK}

# Download FreeDOS and copy data to the USB key

I used the FreeDOS 1.2 lite installation image you can find on the FreeDOS site (`FD12LITE.zip`). From that archive copy `FD12LITE.img` to the USB key

    cp FD12LITE.img ${MOUNTPOINT}

You also need `memdisk` from syslinux.

    cp /usr/lib/syslinux/memdisk ${MOUNTPOINT}

And finally, you need to copy the BIOS update tool (or whatever DOS application you want to run)

    mkdir ${MOUNTPOINT}/bios
    cp -a path/to/bios_update_tool ${MOUNTPOINT}/bios

# Create a grub.cfg

The grub.cfg must be at `${MOUNTPOINT}/boot/grub/grub.cfg`. 

    set timeout=20
    set default=0

    menuentry 'FreeDOS' {
        set root='(hd0,msdos1)’
	    linux16 /memdisk
	    initrd16 /FD12LITE.img
    }

# Boot and run

Now everything is ready to boot from the USB stick. Once you boot FreeDOS, the `${MOUNTPOINT}` directory is accessible as `D:\`.

# References

https://colinxu.wordpress.com/2018/12/29/create-a-universal-bootable-usb-drive-using-grub2/

http://tuxtweaks.com/2009/05/create-a-bootable-usb-drive-ubuntu-freedos/

https://gist.github.com/jamiekurtz/26c46b3e594f8cdd453a
