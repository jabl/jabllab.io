---
title: Reducing size of PDF files
author: Janne
date: 2023-06-19
draft: false
tags: ["PDF", "Linux", "Ghostscript"]
---

# Introduction

I was recently faced with the problem of having to upload a PDF document, and
there was a very strict limit on the size of the document.

# Reducing size of images

The main way this can be done is by reducing the resolution of images. Text
usually compresses quite well, and it's not possible to have lossy compression.
So the main remaining avenue is reducing the size of images by reducing their
resolution or compressing them more.

As a caveat, for scanned PDF documents, where pages are stored as images, this
can result in unreadable text!

# Ghostscript to the rescue

Ghostscript contains the necessary tools to accomplish this. 

## -dPDFSETTINGS=

The Ghostscript `-dPDFSETTINGS=` sets a number of settings affecting the size.
`/screen` results in the smallest size. Run it like the following, to convert
the file `input.pdf` to a hopefully significantly smaller `output.pdf`.

```
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFSETTINGS=/ebook \
   -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
```

## Even smaller size

`-dPDFSETTINGS=/screen` sets the DPI of images to 75. One can reduce the size
further with various `-dXXXResulution=` settings. There are also some other
useful settings shown below, mostly self-explanatory:

```
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFSETTINGS=/screen \
   -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages=true \
   -dSubsetFonts=true -dConvertCMYKImagesToRGB=true -dCompressFonts=true \
   -dDownsampleColorImages=true -dDownsampleGrayImages=true \
   -dDownsampleMonoImages=true -dColorImageResolution=60 \
   -dGrayImageResolution=60 -dMonoImageResolution=60 \
   -sOutputFile=output.pdf input.pdf
```

# Conclusions

Hopefully the above helps. Also, if you need to create a combined document
consisting of multiple PDF's (pdftk is an excellent tool for this!), you can
use different settings for different parts of the final document. For instance,
if one of the inputs is a scanned PDF with text as images, you probably want to
  reduce the size of this one less than other input documents.
