---
title: Ode to the color white
author: Janne
date: 2023-05-19
draft: false
tags: ["color", "white"]
---

# Introduction

This is a short reflection (ha!) of what makes the color white special and
useful from a practical and safety perspective. I make no claims about whether
white is, or should be, fashionable at the time of writing this.

# What is color, anyway?

To be very brief, color is what we ascribe to electromagnetic radiation of
specific wavelengths in the visible part of the spectrum. For instance, the
color red corresponds roughly to wavelengths of [625 - 740 nm whereas the
shortest wavelengths of the visible spectrum correspond to the color violet at
about 380-450nm][wikispectrum].

# What about the colors white and black, then?

From the [list of colors in the visible spectrum][wikispectrum] we see familiar
colors like red, blue and green. But not white or black. So what are these?
White is a combination of all the colors in the visible spectrum. So a white
light means electromagnetic radiation in all frequencies in the visible
spectrum. Black is, on the other hand, the absence of light.

# What does it mean when an object has a specific color?

The above describes properties of electromagnetic radiation, or light sources.
So what does it mean when an object which is not a light source itself, has a
specific color? In short, all objects we can see we see because light from some
light source hits them and some of that radiation is reflected back into our
eyes. So in a way all objects we see are light sources, although of course much
weaker than a lamp or the sun. However, objects don't reflect all wavelengths
equally, which is the key in determining their color. When a white light source
shines on a red object, what happens is that of the incoming white light (which
as mentioned above, is a mixture of all visible wavelengths) most of the light
is absorbed in the object and only red light is reflected back towards the
observer. A green object absorbs all light except green which is reflected
back. And so on.

A white object, then, is an object which reflects back all the incoming
radiation. And a black object is an object which absorbs all the incoming
radiation.

# So what is the difference between a white object and a mirror?

If a white object reflects all the incoming radiation, isn't it then a mirror
and not a white object? No, the difference is due to diffuse vs. specular
reflection. Specular reflection is what you see in mirrors, and what you
calculated when you had optics in high school physics. Diffuse reflection,
which is what happens for a white object, is when the object reflects the
radiation back in all directions.

Diffuse and specular reflection is also what distinguishes glossy and matte
surfaces. Glossy surfaces have a higher fraction of specular reflection, and
matte surfaces have a higher fraction of diffuse reflection.

# Reflectance

Reflectance means the proportion of incoming radiation that is reflected back
by a surface. So a reflectivity of 100% means a surface reflects all the
incoming radiation back, whereas a reflectivity of 0% would mean a perfectly
black surface that adsorbs all the incoming radiation.

So how does the color then affect the reflectivity?

Table from [The Tankship Tromedy][tromedy]. This is about common ship paints
for steel hulls, presumably results would be slightly different for other types
 of paints although the broad outlines would presumably still apply.

| COLOUR               | REFLECTANCE |
|----------------------|-------------|
|Black RAL 9005        |  3%         |
|Machine Grey RAL 7031 | 10%         |
|Silver Grey RAL 7001  | 27%         |
|Red RAL 3001          | 43%         |
|Light Grey RAL 7035   | 51%         |
|Cream White RAL 9010  | 72%         |
|White RAL 9001        | 84%         |

One can see that there is a very large difference in reflectivity between white
and black, with other colors inbetween. However, one can also see that a very
small fraction of non-white color drastically reduces the reflectivity. A light
gray color already has a reflectance of only 51% compared to 84% for a clear
white.

# Safety colors and white

The safety advantage of white comes from the reflectance. Being the most
reflective color white is also the most visible in low light conditions. That
being said, visibility is not only about the maximum reflectance but also about
contrast with the background. And in that respect there are certainly
situations where white isn't the optimal. For instance, safety equipment at sea
tends to be orange, in order to provide contrast against white foam on a stormy
sea. Orange is also a complimentary color to the azure color of the sky, and
hence stands out against a sky background. 

In addition to orange, another common safety color is some kind of lime yellow,
or greenish light yellow. This is frequently seen on e.g. emergency vehicles
following [research starting in the 1990'ies][limeyellow]. 

# Cars

An Australian study [from 2007][carcrashrisk2007] (later republished as [an
article][carcrashrisk2010]) found that white cars have a the statistically
lowest chance of being involved in accidents. Though in Australia it's
apparently common to drive with the headlights off during the day, which
probably increases the influence of the color of the car. White is also a
popular color in hot climates due to the high reflectivity meaning that a white
car doesn't get as hot in the sun as one of a darker color.

# Ships

In [The Tankship Tromedy][tromedy] the author argues that white is the best
color for an oil tanker. A white coating keeps the deck temperature well below
the glass transition temperature of the paint, prolonging the life of the
paint. Also white helps keep the ship clean and tidy, as rust spots and dirt
are immediately visible. A short version of the core argument at
[gcaptain][ships].

# Aircraft

Similar arguments as for ships above have been made for aircraft. White keeps
the aircraft cooler, reducing air conditioning requirements, and helps spot
issues. Also most commercial aircraft are leased, and airlines can save a lot
of money by not needing to repaint aircraft at the beginnig and end of the
lease period, as white is the most common color.

# Cities

For cities, or buildings in general, [cool surfaces][coolsurf] reduces air
conditioning costs in warm climates as well as the urban heat island effect,
where lots of dark surfaces like roads and building soak up solar energy.

# Appendix: Isn't it spelled colour and not color?

Color is the US English spelling, whereas British English uses colour.

[wikispectrum]: https://en.wikipedia.org/wiki/Visible_spectrum
[ships]: https://gcaptain.com/whats-the-best-color-to-paint-a-ship-the-answer-might-surprise-you/
[tromedy]: http://www.c4tx.org/ctx/pub/tromedy2.pdf
[limeyellow]: https://www.apa.org/topics/safety-design/fire-engine-color-safety
[coolsurf]: https://www.climateresolve.org/reflecting-sunlight/
[carinsurance]: https://www.comparethemarket.com.au/blog/car/does-the-colour-of-your-car-affect-insurance/
[carcrashrisk2007]: https://www.monash.edu/__data/assets/pdf_file/0007/216475/An-investigation-into-the-relationship-between-vehicle-colour-and-crash-risk.pdf
[carcrashrisk2010]: https://doi.org/10.1016/j.ssci.2010.05.001
