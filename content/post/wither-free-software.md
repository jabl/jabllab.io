---
title: Wither Free Software?
author: Janne
date: 2021-01-22
draft: false
tags: ["FOSS", "licensing"]
---

(*Updated 2022-01-02, originally published 2021-01-22*)

# Introduction

Lately I've been reading and thinking about FOSS licensing, and how it's
evolved and where it's going. Specifically the never-ending battle between Free
Software and Open Source.  To begin with, let me very briefly describe what I'm
talking about; if you're familiar with Free Software vs. Open Source and
Copyleft vs Permissive licensing feel free to skip the following two sections.

## Recap: Free and Open Source Software

Free Software is defined as software which fulfills the [four
freedoms](https://www.gnu.org/philosophy/free-sw.html.en), whereas Open Source
is defined by the [Open Source Definition](https://opensource.org/docs/osd).
While the definitions are to an extent similar, typically the real diffference
between Free Software and Open Source proponents is more along moral lines.
Free Software proponents generally tend to think of Free Software as an ethical
issue, whereas Open Source tends to take the side that Open Source is merely a
better, more efficient way to produce software, without saying that proprietary
source per se is unethical.

## Recap: Copyleft vs Permissive licenses

In their quest to free all software, the Free Software camp invented the
copyleft license, an ingenious twist on a traditional copyright license.  The
gist is that copyleft licenses are viral; if you use copyleft code as part of
your project, you need to distribute your own license under a
copyleft-compatible license as well.  The goal thus being an ever-increasing
amount of copyleft code, and if you want to use that code, your code needs to
join the copyleft pool as well.

Permissive licenses, OTOH, tend to be more like "do whatever you want".  They
place minimal restrictions on what the licensee may do with the code.

Typical arguments tend to be that the Free Software camp prefers copyleft
licenses to prevent freeloaders who would take free software and build
proprietary products, whereas the Open Source camp argues that it's fine to use
open source code to create proprietary products, and improvements to the open
source code would naturally flow back to the original open source project due
to the superior economic efficiency of sharing the maintenance burden.

There exists a middle ground between these two, namely so-called "weak
copyleft" licenses, where the copyleft is limited to the project itself, not
code that uses it. For example, a library under a weak copyleft license can be
used by a proprietary application without having to distribute the source code
of the application itself under a copyleft license, however modifications to
the library itself must be distributed in source form. Popular examples of weak
copyleft licenses are the LGPL, where the demarcation line for the copyleft is
the project, typically a library, and MPL and EPL which use a file level
copyleft.

# The decline of Free Software

Regularly we see studies and articles by various people suggesting the demise
of copyleft licensing vs. permissive licenses (e.g.
[whitesource][whitesource]). That might be correct, although one
counterargument is that the vast majority of these new permissive projects are
one-off scripts soon forgotten, whereas copyleft licenses make up a larger
share among bigger more established projects. However, even if this was
correct, it points to a worrying trend, as every project starts small.

Note that the decline of copyleft is mostly a relative decline vs permissive
licenses, the total body of FOSS is still increasing at a fast clip.

Why is then copyleft declining, even if only relatively? It seems Open Source
has "stolen" the limelight and has become a big part of the entire software
industry, whereas Free Software still is a relatively small activist movement.
One explanation is that businesses have largely shunned the ethical aspects of
Free Software, and the entire Open Source movement was in fact started in order
to make what was then known as free software more acceptable to business. Thus,
a bigger share of corporate contributions to the Open Source ecosystem partly
explains the rising share of Open Source vs Free Software.

Another more unfortunate reason for the demise of free software vs open source
is that changes in the landscape has made copyleft licensing irrelevant in many
cases. On the smaller end of the spectrum, the rejection of GPLv3 with its
"anti-Tivoization" clause by most critical projects (in particular, the Linux
kernel) has meant that locked down devices powered by ostensibly free software
have become increasingly common. And also, many embedded devices are made by
short-lived drive-by operations in some far-away country; good luck prosecuting
them for GPL violations. On the other end of the spectrum, the wholesale
rejection of AGPL by, well, more or less everybody, has enabled the SAAS
providers to "proprietarize" free software. For more about this argument, see
[Jeremy Allison's talk at copyleftcon2020][allison]. So all in all, in many
cases the stronger copyleft licenses do not in practice provide the protection
that they originally were developed for. Efforts have been made to make
[stronger copyleft licenses][kemitchell], but they have not generally been
accepted as open source, so it remains to be seen what space there is for
strengthening copyleft.

# Sustainable Open Source

As Free Software is going through it's own crisis of relevance in the modern
world, Open Source is going through a [midlife crisis][midlife] of its own. The
Open Source crisis is mostly related to [how to make money with Open
Source][stackoverflow]. Many companies making infrastructure software like
database engines have switched to licenses even more draconian than the Affero
GPL in an effort to prevent cloud providers like AWS to "operationalize" their
software and offering it to their customers, cutting out the developers from
any revenue.

To begin with, trite as it may sound, Open Source is not a business model. The
universe doesn't owe anyone an income for the act of creating Open Source
software. That being said, if we want to see Open Source software being created
outside of hobbyists or academics, ways must be found to ensure that an Open
Source community can sustain itself economically. If the software is given away
for free, it just means that other ways of earning money must be devised. A
good overview of open source business models can be seen in the [The
Sustainable Free and Open Source Communities Book][sfoscbook], and different
kinds of Open Source communities are [explained by Mozilla][mozilla]. A short
overview of some common models:

## Donations

This is where the developers solicits voluntary donations for supporting the
development of the software, or participates in some kind of bounty system
similar to kickstarter. A few platforms that have appeared for this are e.g.

- https://github.com/sponsors
- https://opencollective.com/
- https://www.buymeacoffee.com/
- https://www.oss.fund/

I think this is a nice low overhead method of gathering a little bit of money
on the side for moderately popular small projects, but is unlikely to provide
full time income not to mention an income stream to sustain a business built
around creating the software. Also many businesses apparently have very
convoluted ways of paying voluntarily for something they can get for free, so
this approach most likely appeals more to end users.

## Open core

This is a model where you have a 'core', which is fully open source, but then
the company earns money from proprietary add-ons to the core. This model seems
fairly successful and common, see e.g. the [Commercial Open
Source](https://coss.media/) site and the COSS spreadsheet with valuations they
maintain. Although if one is concerned about the big cloud providers
operationalizing the software they might just reimplement whatever proprietary
add-ons they need and just use the open core product.

## Dual licensing

Where the software is offered under the choice of an open source license, or
alternatively a proprietary license for money in cases where the user does not
want to be bound by the open source license. The chosen open source license
tends to be a strict copyleft license like the GNU AGPLv3, in order to drive as
many users as possible to buy the proprietary license. Familiar products using
this strategy are the MySQL database software and the Qt GUI library.

In order to dual license, the vendor must require outside contributors to
assign their copyrights. This is usually a big stumbling block, as outsiders
see themselves as being taken advantage of to further the bottom line of the
vendor. This in this model it's very hard to form a community around the
software, as the copyright holder is 'more equal than the others'.

Many in the copyleft licensing community nowadays think that dual licensing is
poisonous, and should have never been allowed in the first place. One way to
prevent it is the concept of inbound=outbound licensing as [explained by
Bradley Kuhn][sfconservancy].

## Services, support, or "Open Source Product"

In this model all the software is OSS, and the vendor earns an income through
providing services and support, or they can sell a specially vetted version of
the software as an "Enterprise" version with a long support period. This is
probably the oldest and most pure open source business model, with Red Hat the
most visible champion.

The success of Red Hat notwithstanding, and while this model certainly keeps a
fair amount of people employed, it's usually [not a fantastic business model
due to commodification and thus poor pricing power][techcrunch-rh].

## The commodify your complement game

Commodify your complement is a business strategy where a business tries to
commodify products and services that are complementary to their own offering,
in order to bring in a larger share of the total spending in that market
segment. For instance, cars and fuel are complementary to each other. In the IT
sector, this strategy has been widely used, though the concept was popularized
by [Joel Spolsky][spolsky]. This is also the essential point made by [Bryan
Cantrill][bmc-economics]. A more recent overview [here][gwern].

I think this model is widely used, and explains many corporate Open Source
contributions pretty well. It can also be relatively immune to many of the
challenges described above, as the company doing it doesn't open source their
core product or service, but rather ancillary software.

## Maintenance sharing

This is another common model, often done via a foundation. A prominent example
being the Linux Foundation which is financed by industry and, among other
things, pays the salaries of Linus Torvalds and some other prominent Linux
kernel developers. Participants benefit from sharing the maintenance burden on
software they all depend on, but which aren't differentiating features in their
offerings.

# What is the role for copyleft?

The above paints a somewhat bleak future for copyleft. Copyleft has
historically had some nice successes in opening code that would otherwise been
closed, such as Apples Objective C frontend to GCC, or the [WiFi driver for the
WRT54G][wrt54g] that kickstarted the successful [OpenWrt](https://openwrt.org/)
project. However, copyleft lawsuits also have quite a big negative impact on
the ecosystem, as it makes potential contributors apprehensive or maybe even to
completely use some other code. For instance, the former Busybox maintainer was
one of the plaintiffs in the Busybox lawsuits, and in the end he became so
disgusted to the whole show that he created a permissively licensed alternative
called [toybox][toybox].

As today a lot of Open Source is created in order to share the maintenance
burden of some infrastructure type code such, or by companies playing the
commodify your complement game, it seems that a resurgence of copyleft is not
in sight. In fact, it seems that already today, the only really critical
copyleft project left is the Linux kernel, and since the syscall interface
provides a boundary over which the copyleft 'virality' doesn't escape, the
effect of Linux on the larger adoption of copyleft in user-space software is
rather minimal.

A common argument in favor of copyleft is "tit for tat", or preventing
freeloaders. I think this is an intuitive argument, but I'm not sure it
actually stands up to more thorough investigation. The crucial question is,
what is gained by pushing freeloaders away? For a hypothetical example, say I
produce some software and I release it under a strong copyleft license, in
order to avoid freeloaders. If some potential user doesn't want to abide by the
license restrictions, what are the potential scenarios?

- The user reconsiders the licensing of their own code, and releases their own
code under a copyleft compatible license.

- The user buys a commercial license from me in order to escape the copyleft
licensing provisions (see the dual licensing section above).

- The user instead uses another piece of software that does roughly the same as
my software, but under a more permissive license.

- The user instead reimplements the functionality they desired from my
software.

- The user uses my code without complying with the license. Easy enough if they
reside in some far-away country, or are a large corporation with a lot of money
and lawyers.

To be honest, I think the first two options, where copyleft licensing achieves
its goals, are somewhat unrealistic (and the second one, as explaine above, is
in many ways quite problematic, leading to many calling it outright poisonous).
Thus, if there is little, or no, value in pushing freeloaders away, it makes
more sense to aim for maximizing adoption by instead using a permissive license
rather than a copyleft one.

One could argue that weak copyleft licenses have a part to play here. They
won't scare away freeloaders, and while weak copyleft licenses as such are
easier to circumvent for the determined leecher, they at least state a
"community ethos" that improvements are expected back.

# Conclusions

To conclude with some personal reflections, yes, I do think copyleft has to
some extent become obsolete. That doesn't mean that copyleft software won't be
made in the future, certainly people are free to use whatever license they
wish, and if they wish to use a copyleft license then so be it. But the
heydays of copyleft are over, and are not coming back. I think the social goals
championed by Free Software advocates are better advanced through other means,
largely political activism. A few examples:

- Right to repair legislation, to give people power of their own devices, and
allowing small repair shops to repair devices rather than only the
manufacturer.
- Documented, interoperable protocols (via lawsuits if not otherwise) as
advocated by [Jeremy Allison][allison] as an alternative to ever more stringent
strong copyleft licenses like AGPL.
- Data portability and anti-trust legislation etc. to reduce the political and
economic power of the web giants.

So what about monetizing open source then? I think there are a few ways:

- For a simple projects with no prospects of really selling it; put up a
donations page. It won't make you rich, most likely won't even enable you to
sustain yourself economically, but could be better than nothing.
- Sharing the maintenance burden, or playing the commodify your complement
game. Yes, this is good. A lot of companies playing the commodify your
complement game against each others will continually increase the quantity and
quality of the open source software pool. Or taking this argument [a step
further][bmc-economics], in the long run companies playing this game against
each other, and thanks to the marginal cost of software being approximately
zero, all software will be zero cost. Thus companies that are able to benefit
from the commodify your complement game will prosper, whereas companies trying
to monetize software directly will suffer.
- Open sourcing the core assets of your company. No, this is a bad idea! In
this case, just make the software proprietary or maybe open core. But keep in
mind the commodify your complement game; you must continually innovate lest the
free options eat your lunch!

So what about myself then? I think by default when writing new code (as opposed
to contributing to some existing project), I'll use the weak copyleft MPL-2.0.
It's a "modern" license with things like patent protections, it's compatible
with (L)GPL 2 & 3, as well as the popular Apache 2.0 license. While the weak
per-file copyleft is relatively easy to circumvent for the determined
freeloader, at least it makes a statement that improvements to the project
itself are expected back and that otherwise using it for whatever purposes,
including making proprietary software, is ok. Other projects using MPL-2.0 are
the various Mozilla projects (Firefox, Thunderbird, etc.), LibreOffice, and
recently also ISC projects (the Bind DNS server, ISC DHCP, etc.) were
relicensed from the permissive ISC license to MPL-2.0.

[whitesource]: https://resources.whitesourcesoftware.com/blog-whitesource/open-source-licenses-trends-and-predictions
[allison]: https://archive.org/details/copyleftconf2020-allison
[kemitchell]: https://writing.kemitchell.com/2018/11/04/Copyleft-Bust-Up.html
[stackoverflow]: https://stackoverflow.blog/2021/01/07/open-source-has-a-funding-problem
[midlife]: http://dtrace.org/blogs/bmc/2018/12/14/open-source-confronts-its-midlife-crisis/
[bmc-economics]: http://dtrace.org/blogs/bmc/2004/12/16/the-economics-of-software-redux/
[sfoscbook]: https://sfosc.org/docs/book/
[mozilla]: https://blog.mozilla.org/wp-content/uploads/2018/05/MZOTS_OS_Archetypes_report_ext_scr.pdf
[sfconservancy]: https://sfconservancy.org/blog/2020/jan/06/copyleft-equality/
[techcrunch-rh]: https://techcrunch.com/2014/02/13/please-dont-tell-me-you-want-to-be-the-next-red-hat
[spolsky]: https://www.joelonsoftware.com/2002/06/12/strategy-letter-v/
[gwern]: https://www.gwern.net/Complement
[wrt54g]: https://tedium.co/2021/01/13/linksys-wrt54g-router-history/
[toybox]: http://landley.net/toybox/about.html
